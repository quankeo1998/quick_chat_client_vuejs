import './assets/scss/app.scss'
import './assets/js/app.js'
import axios from "axios";
import { createApp } from 'vue'
import { createPinia } from 'pinia'

import App from './App.vue'
import router from './router'
import Echo from "laravel-echo";
import Pusher from "pusher-js";
const app = createApp(App)
// window.echo =new Echo({
//     broadcaster: 'Pusher',
//     key:import.meta.env.VUE_APP_PUSHER_APP_KEY,
//     wsHost:'127.0.0.1',
//     wsPort:6001,
//     cluster:"mt1",
//     disableStats:true,
//     forceTLS:false
//
// })
app.use(createPinia())
app.use(router)

app.mount('#app')
