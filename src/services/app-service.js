import axios from "axios";
export const appService = axios.create({
    baseURL : import.meta.env.VITE_API_URL
})
class AppService {
    login(data){
        return appService.post('api/auth/login',data)
    }
    register(data)
    {
        return appService.post('api/auth/register',data)
    }
    logOut()
    {
        return appService.get('api/auth/logout',{
            headers:{
                Authorization:'Bearer '+ localStorage.getItem('token')
            }
        })
    }
    getUser(){
        return appService.get('api/user',{
            headers:{
                Authorization:'Bearer '+ localStorage.getItem('token')
            }
        })
    }
    editProfile(data){
        return appService.post('api/user/update',data,{
            headers:{
                Authorization:'Bearer '+ localStorage.getItem('token')
            }
        })
    }
    addConversation(data,user_id){
        return appService.post('api/conversations/store/'+user_id,data,{
            headers:{
                Authorization:'Bearer '+ localStorage.getItem('token')
            }
        })
    }
    getConversation(){
        return appService.get('api/conversations',{
            headers:{
                Authorization:'Bearer '+ localStorage.getItem('token')
            }
        })
    }
    getConversationDetail(conversaton_id){
        return appService.get('api/conversations/'+conversaton_id,{
            headers:{
                Authorization:'Bearer '+ localStorage.getItem('token')
            }
        })
    }
    removeConversation(conversation_id){
        return appService.delete('api/conversations/delete/'+conversation_id,{
            headers:{
                Authorization:'Bearer '+ localStorage.getItem('token')
            }
        })
    }
    //Xóa toàn bộ tin nhắn trong cuộc trò chuyện
    removeMessageConversation(conversation_id){
        return appService.delete('api/conversations/delete-message/'+conversation_id,{
            headers:{
                Authorization:'Bearer '+ localStorage.getItem('token')
            }
        })
    }
    getMessage(conversation_id){
        return appService.get('api/conversations/message/'+conversation_id,{
            headers:{
                Authorization:'Bearer '+ localStorage.getItem('token')
            }
        })
    }
    sendMessage(data,conversation_id){
        return appService.post('api/conversations/message/store/'+conversation_id,data,{
            headers:{
                Authorization:'Bearer '+ localStorage.getItem('token')
            }
        })
    }
    //Xác nhận đã đọc tin nhắn
    readMessage(conversation_id){
        return appService.put('api/conversations/message/read/'+conversation_id,'',{
            headers:{
                Authorization:'Bearer '+ localStorage.getItem('token')
            }
        })
    }
    //xóa tin nhắn của chính mình
    deleteMessage(message_id){
        return appService.delete('api/conversations/my-message/delete/'+message_id,{
            headers:{
                Authorization:'Bearer '+ localStorage.getItem('token')
            }
        })
    }
    getContact(){
        return appService.get('api/contacts',{
            headers:{
                Authorization:'Bearer '+ localStorage.getItem('token')
            }
        })
    }
    getContactDetail(user_id){
        return appService.get('api/contacts/user/'+user_id,{
            headers:{
                Authorization:'Bearer '+ localStorage.getItem('token')
            }
        })
    }
    addContact(data){
        return appService.post('api/contacts/create',data,{
            headers:{
                Authorization:'Bearer '+ localStorage.getItem('token')
            }
        })
    }
    removeContact(contact_id){
        return appService.delete('api/contacts/delete/'+contact_id,{
            headers:{
                Authorization:'Bearer '+ localStorage.getItem('token')
            }
        })
    }
    searchUser(data){
        return appService.get('api/user/search?keyword='+data,{
            headers:{
                Authorization:'Bearer '+ localStorage.getItem('token')
            }
        })
    }
    createGroup(data){
        return appService.post('api/conversations/store-group',data,{
            headers:{
                Authorization:'Bearer '+ localStorage.getItem('token')
            }
        })
    }
    inviteUserGroup(data,conversations_id){
        return appService.post('api/conversations/invite/'+conversations_id,data,{
            headers:{
                Authorization:'Bearer '+ localStorage.getItem('token')
            }
        })
    }
    removeUserGroup(conversation_id,user_id){
        return appService.post('api/conversations/removeMember/'+conversation_id+'/'+user_id,'',{
            headers:{
                Authorization:'Bearer '+ localStorage.getItem('token')
            }
        })
    }

}
export const httpService = new AppService()
